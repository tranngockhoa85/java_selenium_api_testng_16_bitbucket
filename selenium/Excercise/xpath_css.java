package Excercise;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class xpath_css {
	WebDriver driver;

	@BeforeClass
	public void setup() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://live.demoguru99.com/");

	}

	@Test
	public void TC01_loginwithemptyemailandpassword() {
		// open my account page
		driver.findElement(By.linkText("MY ACCOUNT")).click();

		// input email
		driver.findElement(By.xpath("//input[@title='Email Address']")).sendKeys("");

		// input password
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("");

		// click login button
		driver.findElement(By.id("send2")).click();
		
		//verify text
		String emailErrorMessage = driver.findElement(By.xpath("//div[@id='advice-required-entry-email']")).getText();
		String passwordErrorMessage = driver.findElement(By.id("advice-required-entry-pass")).getText();
		System.out.println(emailErrorMessage);
		System.out.println(passwordErrorMessage);
		Assert.assertEquals(emailErrorMessage, "This is a required field.");
		Assert.assertEquals(passwordErrorMessage, "This is a required field.");
	}

	@Test
	public void TC02_Loginwithinvalidemail()
	{
		//open my account page
		driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
		
		//input email
		driver.findElement(By.id("email")).sendKeys("12314214131@1232.123");
		
		//input password
		driver.findElement(By.id("pass")).sendKeys("");
		
		//click login button
		driver.findElement(By.id("send2")).click();
		
		//verify text
		String emailErrorMessage = driver.findElement(By.id("advice-validate-email-email")).getText();
		Assert.assertEquals(emailErrorMessage, "Please enter a valid email address. For example johndoe@domain.com.");	
	}
	
	@Test
	public void TC03_Loginwithpasswordless6characters()
	{
		//open my account page
		driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
		
		//input email
		driver.findElement(By.id("email")).sendKeys("automation@gmail.com");
		
		//input password
		driver.findElement(By.id("pass")).sendKeys("123");
		
		//click login button
		driver.findElement(By.id("send2")).click();
		
		//verify text
		String passwordErrorMessage = driver.findElement(By.id("advice-validate-password-pass")).getText();
		Assert.assertEquals(passwordErrorMessage, "Please enter 6 or more characters without leading or trailing spaces.");	
	}

	@Test
	public void TC03_Loginwithincorrectpassword()
	{
		//open my account page
		driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
		
		//input email
		driver.findElement(By.id("email")).sendKeys("automation@gmail.com");
		
		//input password
		driver.findElement(By.id("pass")).sendKeys("123231412");
		
		//click login button
		driver.findElement(By.id("send2")).click();
		
		//verify text
		String passwordErrorMessage = driver.findElement(By.xpath("//li[@class='error-msg']//span")).getText();
		Assert.assertEquals(passwordErrorMessage, "Invalid login or password.");	
	}
	
	@AfterClass
	public void CloseBrowser()
	{
		driver.quit();
	}
}
