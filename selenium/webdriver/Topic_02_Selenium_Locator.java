package webdriver;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Topic_02_Selenium_Locator {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://live.demoguru99.com/index.php/customer/account/create/");
	}

//	@Test
//	public void TC_01_ID() {
//		driver.findElement(By.id("firstname")).sendKeys("Khoa");
//		sleepinseconds(3);
//	}
//
//	@Test
//	public void TC_02_Name() {
//		driver.findElement(By.name("lastname")).sendKeys("Trần");
//		sleepinseconds(3);
//	}
//	
////	@Test
//	public void TC_03_Class() {
//		driver.get("http://live.demoguru99.com/index.php/customer/account/login/");
//		driver.findElement(By.className("validate-password")).sendKeys("123456");
//		sleepinseconds(3);
//	}
//
//
//	
//
//	@Test
//	public void TC_04_Tagname() {
//		int countTagname = driver.findElements(By.tagName("input")).size();
//		System.out.println("Number of Tagname 'input' : " + countTagname);
//		sleepinseconds(3);
//
//	}
//
//	@Test
//	public void TC_05_LinkText() {
//		driver.findElement(By.linkText("ABOUT US")).click();
//		sleepinseconds(3);
//	}
//
//	@Test
//	public void TC_06_Partial_LinkText() {
//		driver.findElement(By.partialLinkText("POLICY")).click();
//		sleepinseconds(3);
//	}

//	@Test
//	public void TC_07_Css() {
//		driver.get("http://live.demoguru99.com/index.php/customer/account/create/");
//		// ID
//		driver.findElement(By.cssSelector("#firstname")).sendKeys("khoa");
//
//		// Name
//		driver.findElement(By.cssSelector("input[name='middlename']")).sendKeys("ngoc");
//
//		// Class	
//		boolean a = driver.findElement(By.cssSelector(".name-lastname")).isDisplayed();
//		System.out.println(a);
//
//		// Tagname
//		System.out.println("Number of input tag = " + driver.findElements(By.cssSelector("input")).size());
//		// link text
//		driver.findElement(By.cssSelector("a[href='http://live.demoguru99.com/index.php/tv.html']")).click();
//
//		// Partial linktext
//		driver.findElement(By.cssSelector("a[href*='index.php/mobile.html']")).click();
//
//		sleepinseconds(5);
//	}

	
	@Test
	public void TC_08_Xpath() {
		driver.get("http://live.demoguru99.com/index.php/customer/account/create/");
		
		// ID
		driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("khoa");
		
		// Name
		driver.findElement(By.xpath("//input[@name='middlename']")).sendKeys("ngoc");
		// Class	
		driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
		// Tagname
		// link text
		// Partial linktext
	}
	
//	@AfterClass
	public void afterClass() {
		driver.quit();

	}

	public void sleepinseconds(long timeout) {
		try {
			Thread.sleep(timeout * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
