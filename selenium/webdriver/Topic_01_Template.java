package webdriver;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Topic_01_Template {
	WebDriver driver;

	@BeforeTest
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test
	public void TC_01_l() {
		// Login Page Url matching
		driver.get();
	}

	@Test
	public void TC_02_() {
		driver.get();
	}

	@Test
	public void TC_03_() {
		driver.get();
	}

	@AfterTest
	public void afterClass() {
		driver.quit();
		
	}

}


